import Proxy from './BaseProxy';

class ArtistProxy extends Proxy {
  /**
   * The constructor for the ArtistProxy.
   *
   * @param {Object} parameters The query parameters.
   */
  constructor(parameters = {}) {
    super('api/artists', parameters);
  }
}

export default ArtistProxy;
