export const Base = require('./base/base.vue');
export const Minimal = require('./minimal/minimal.vue');

export default {
  Base,
  Minimal,
};
