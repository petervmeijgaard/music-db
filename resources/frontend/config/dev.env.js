var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_LOCATION: '"http://music-db.dev"',
  API_CLIENT_ID: "'2'",
  API_CLIENT_SECRET: '"cKG8naCv4yCH4LXvhKY1qzuVok7EX2GJ1eLmVS1d"',
  BROADCAST_ENDPOINT: '"http://music-db.dev/broadcasting/auth"',
  PUSHER_KEY: '"2944b7b2ff1a1ae1a096"',
  PUSHER_CLUSTER: '"eu"'
})
